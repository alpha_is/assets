/**
 * Modal widnow for Bootstrap 4
 * @author Lukáš Černý
 */
const Modal = ((document, window) => {
    'use strict';

    const EVENTS = {
        HIDE: 'hide',
        HIDE_PRE: 'pre_hide'
    };

    class Modal {
        constructor(id, content, config) {
            this._modal = undefined;
            this._backdrop = undefined;
            config = Object.assign({texts: {}, config: {}, elements: {}}, config);
            this._config = {
                id: id,
                class: 'modal fade',
                content: content,
            };
            this._options = {
                texts: Object.assign({
                    title: 'TITLE',
                    closeButton: 'Close'
                }, config.texts),
                config: Object.assign({
                    closeButton: true,
                    footerButtons: []
                }, config.config),
                elements: Object.assign({
                    title: 'h4'
                }, config.elements)
            };
            this._generateModal();
            this._generateBackdrop();
        }

        static get eventHide() {
            return EVENTS.HIDE;
        }

        static get eventHidePre() {
            return EVENTS.HIDE_PRE;
        }

        get modal() {
            return this._modal.modal;
        }

        show() {
            if (typeof(this._modal) !== 'undefined') {
                document.body.classList.add('open-modal');
                var toShow = this.modal;
                if (!toShow.parentNode
                    || toShow.parentNode.nodeType !== Node.ELEMENT_NODE) {
                    document.body.appendChild(toShow);
                    document.body.appendChild(this._backdrop);
                }
                toShow.style.display = 'block';
                toShow.classList.add('show');
                this._backdrop.classList.add('show');
            }
        }

        hide(event) {
            if (event) {
                event.preventDefault();
            }
            if (typeof (this._modal) !== 'undefined') {
                var toHide = this.modal;
                toHide.style.display = 'none';
                this._backdrop.style.display = 'none';
                toHide.setAttribute('aria-hidden', true);
                this._backdrop.setAttribute('aria-hidden', true);
                document.body.classList.remove('modal-open');
                var newEvent = document.createEvent('Event');
                newEvent.initEvent(Modal.eventHide, true, true);
                toHide.dispatchEvent(newEvent);
                toHide.remove();
                this._backdrop.remove();
            }
        }

        _generateModal() {
            var structure = {
                title: document.createElement(this._options.elements.title),
                header: document.createElement('div'),
                body: document.createElement('div'),
                footer: document.createElement('div'),
                content: document.createElement('div'),
                dialog: document.createElement('div')
            };
            for (var key in structure) {
                structure[key].className = 'modal-' + key;
            }
            var that = this;
            structure.title.innerText = this._options.texts.title;
            structure.header.appendChild(structure.title);
            var closeButton = document.createElement('button');
            closeButton.setAttribute('type', 'button');
            closeButton.setAttribute('class', 'close');
            closeButton.dataset.dismiss = 'modal';
            closeButton.innerHTML = '&times;';
            closeButton.addEventListener('click', function () {
                this.hide();
            }.bind(this));
            structure.header.appendChild(closeButton);
            if (typeof(this._config.content) === 'string') {
                structure.body.innerHTML = this._config.content;
            } else {
                structure.body.appendChild(this._config.content);
            }

            structure.content.appendChild(structure.header);
            structure.content.appendChild(structure.body);
            structure.content.appendChild(structure.footer);
            structure.dialog.appendChild(structure.content);
            structure.modal = document.createElement('div');
            structure.modal.className = this._config.class;
            structure.modal.setAttribute('id', this._config.id);
            structure.modal.appendChild(structure.dialog);

            this._options.config.footerButtons.forEach(function (button) {
                structure.footer.appendChild(button);
            });
            if (this._options.config.closeButton) {
                var closeButtonFooter = document.createElement('button');
                closeButtonFooter.setAttribute('type', 'button');
                closeButtonFooter.className = 'btn btn-default';
                closeButtonFooter.dataset.dismiss = 'modal';
                closeButtonFooter.innerHTML = this._options.texts.closeButton;
                closeButtonFooter.addEventListener('click', function () {
                    that.hide();
                });
                structure.footer.appendChild(closeButtonFooter);
            }
            this._modal = structure;
        }

        _generateBackdrop() {
            this._backdrop = document.createElement('div');
            this._backdrop.className = 'modal-backdrop fade';
        }
    }

    if (typeof(window.Modal) === 'undefined') {
        window.Modal = Modal;
    }

    return Modal;
})(document, window);

export default Modal;