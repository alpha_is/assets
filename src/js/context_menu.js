import Base from './base';

const ContextMenu = ((document, window) => {
    class ContextMenu extends Base {
        constructor(invokedOn, config) {
            super();
            this.position = Object.assign({
                left: 0, top: 0
            }, config.position);
            this.items = Object.assign([], config.items);
            this.invokedOn = invokedOn;
            this.contextMenu = undefined;
            this.attached = false;
            this._init();
        }

        _init() {
            let ul = this._getTemplate('list');
            this.contextMenu = ul;

            ul.setAttribute('role', 'menu');
            ul.style.display = 'none';
            this.items.forEach(function (action) {
                var item;
                if (action.type === 'divider') {
                    item = this._getTemplate('divider');
                } else {
                    item = this._getTemplate('item');
                    let text = this._getTemplate('innerText'),
                        span = this._getTemplate('icon');
                    if (action.class) {
                        action.class.split(' ').forEach(function (className) {
                            item.classList.add(className);
                        });
                    }
                    text.innerText = action.text;
                    if (action.icon) {
                        action.icon.split(' ').forEach(function (className) {
                            span.classList.add(className);
                        });
                    }
                    item.appendChild(span);
                    item.appendChild(text);
                    this.addEventListener(item, 'click', function (e) {
                        if (action.callback) {
                            action.callback.apply(this, [this.invokedOn, action.id]);
                        }
                        this._clickHandler(e);
                    }.bind(this), this);
                }
                ul.appendChild(item);
            }.bind(this));

            this.addEventListener(this.contextMenu, 'contextmenu', function () {
            });
        }

        _getTemplate(type) {
            let template = {
                list: {
                    elem: 'ul',
                    class: 'context-menu dropdown-menu'
                },
                item: {
                    elem: 'li',
                    class: 'dropdown-item'
                },
                divider: {
                    elem: 'li',
                    class: 'dropdown-divider'
                },
                icon: {
                    elem: 'span',
                    class: 'icon',
                },
                innerText: {
                    elem: 'span',
                    class: ''
                }
            };
            let elem = document.createElement(template[type].elem);
            elem.className = template[type].class;
            return elem;
        }

        _clickHandler(event) {
            this.destroy();
        }

        show() {
            if (this.attached)
                return;

            document.body.appendChild(this.contextMenu);
            this.contextMenu.style.display = 'block';
            this.contextMenu.style.position = 'absolute';
            this.contextMenu.style.left = this.position.left + 'px';
            this.contextMenu.style.top = this.position.top + 'px';
            this.attached = true;

            this.addEventListener(document, 'click', this.destroy, this);
        }

        hide() {
            if (!this.attached)
                return;

            this.removeEventListener(document, 'click', this.destroy, this);

            this.contextMenu.style.display = 'none';
            this.contextMenu.remove();
            this.attached = false;
        }

        destroy() {
            this.hide();
            this.contextMenu = undefined;
        }
    }

    if (typeof (window.ContextMenu) === 'undefined') {
        window.ContextMenu = ContextMenu;
    }

    return ContextMenu;
})(document, window);

export default ContextMenu;