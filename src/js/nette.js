/**
 * Base function for support Nette framework
 * @author Lukáš Černý
 */
const Nette = ((document, window) => {

    class Nette {
        static parseResponse(response) {
            if (response.snippets) {
                Nette.updateSnippets(response.snippets);
            }
            if (response.redirect) {
                Nette.redirect(response.redirect);
            }

        }

        static redirect(location) {
            document.location.replace(location);
        }

        static updateSnippets(snippets) {
            for (var snippet in snippets) {
                var $el = document.getElementById(snippet);
                updateSnippet($el, snippets[snippet]);
            }

            function updateSnippet($el, html) {
                if ($el.querySelectorAll('title').length) {
                    document.title = html;
                } else {
                    if ($el.querySelectorAll('[data-ajax-append]').length) {
                        $el.append(html);
                    } else if ($el.querySelectorAll('[data-ajax-prepend]').length) {
                        $el.prepend(html);
                    } else {
                        $el.innerHTML = html;
                    }
                }
            }
        }
    }

    if (typeof(window.Nette) === 'undefined') {
        window.Nette = Nette;
    }

    return Nette;
})(document, window);

export default Nette;