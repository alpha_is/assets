import Nette from './nette';
/**
 * Base functions
 * @author Lukáš Černý
 */
const Base = ((document, window) => {
    class Base {
        sendRequest(config) {
            config = Object.assign({
                data: {},
                type: 'POST',
                async: true,
                contentType: null,
                success: undefined,
                error: undefined,
                ajax: true,
            }, config);

            var xhr;
            if (window.XMLHttpRequest) {
                xhr = new XMLHttpRequest();
            } else {
                xhr = new window.ActiveXObject('Microsoft.XMLHTTP');
            }
            xhr.open(config.type, config.destination, config.async);
            if (config.contentType) {
                xhr.setRequestHeader('Content-Type', config.contentType);
            }
            if (config.ajax) {
                xhr.setRequestHeader('X-Requested-With', 'XMLHttpRequest');
            }
            xhr.onload = function () {
                if (xhr.status === 200) {
                    var response = JSON.parse(xhr.responseText);
                    Nette.parseResponse(response);
                    if (config.success !== undefined) {
                        config.success(xhr.responseText);
                    }
                } else {
                    if (config.error !== undefined) {
                        config.error(xhr.responseText);
                    }
                }
            };
            if (config.type === 'POST') {
                var data = '';
                if (config.contentType === 'application/json')
                    data = JSON.stringify(config.data);
                else
                    data = config.data;
                xhr.send(data);
            } else {
                xhr.send();
            }
        }

        addEventListener(invokedOn, name, callback, context) {
            if (typeof callback !== 'function') {
                return;
            }
            if (document.addEventListener) {
                invokedOn.addEventListener(name, function (event) {
                    callback.apply(context, arguments);
                    event.preventDefault();
                    return false;
                }, false);
            } else {
                invokedOn.attachEvents('on' + name, function () {
                    callback.apply(context, arguments);
                    window.event.returnValue = false;
                }, false);
            }
        }

        removeEventListener(invokedOn, name, callback, context) {
            if (typeof callback !== 'function') {
                return;
            }
            if (document.removeEventListener) {
                invokedOn.removeEventListener(name, callback.bind(context), false);
            } else {
                invokedOn.attachEvents('on' + name, function () {
                    callback.apply(context, arguments);
                    window.event.returnValue = false;
                }, false);
            }
        }
    }

    return Base;
})(document, window);

export default Base;