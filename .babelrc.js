module.exports = {
    presets: [
        [
            '@babel/env',
            {
                loose: true,
                modules: false,
                exclude: ['transform-typeof-symbol']
            }
        ]
    ],
    plugins: [
        '@babel/plugin-transform-object-assign',
        '@babel/plugin-proposal-object-rest-spread',
        '@babel/plugin-transform-property-mutators'
    ],
    env: {
        test: {
            plugins: ['istanbul']
        }
    }
};