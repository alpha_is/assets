/*
 * Alpha plugins - Nette
 * Builded: Sat Oct 20 2018 10:43:23 GMT+0000 (UTC)
 */
/**
 * Base function for support Nette framework
 * @author Lukáš Černý
 */
var Nette = function (document, window) {
  var Nette =
  /*#__PURE__*/
  function () {
    function Nette() {}

    Nette.parseResponse = function parseResponse(response) {
      if (response.snippets) {
        Nette.updateSnippets(response.snippets);
      }

      if (response.redirect) {
        Nette.redirect(response.redirect);
      }
    };

    Nette.redirect = function redirect(location) {
      document.location.replace(location);
    };

    Nette.updateSnippets = function updateSnippets(snippets) {
      for (var snippet in snippets) {
        var $el = document.getElementById(snippet);
        updateSnippet($el, snippets[snippet]);
      }

      function updateSnippet($el, html) {
        if ($el.querySelectorAll('title').length) {
          document.title = html;
        } else {
          if ($el.querySelectorAll('[data-ajax-append]').length) {
            $el.append(html);
          } else if ($el.querySelectorAll('[data-ajax-prepend]').length) {
            $el.prepend(html);
          } else {
            $el.innerHTML = html;
          }
        }
      }
    };

    return Nette;
  }();

  if (typeof window.Nette === 'undefined') {
    window.Nette = Nette;
  }

  return Nette;
}(document, window);

export default Nette;
