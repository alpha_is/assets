/*
 * Alpha plugins - Modal
 * Builded: Sat Oct 20 2018 10:43:23 GMT+0000 (UTC)
 */
function _defineProperties(target, props) {
  for (var i = 0; i < props.length; i++) {
    var descriptor = props[i];
    descriptor.enumerable = descriptor.enumerable || false;
    descriptor.configurable = true;
    if ("value" in descriptor) descriptor.writable = true;
    Object.defineProperty(target, descriptor.key, descriptor);
  }
}

function _createClass(Constructor, protoProps, staticProps) {
  if (protoProps) _defineProperties(Constructor.prototype, protoProps);
  if (staticProps) _defineProperties(Constructor, staticProps);
  return Constructor;
}

function _extends() {
  _extends = Object.assign || function (target) {
    for (var i = 1; i < arguments.length; i++) {
      var source = arguments[i];

      for (var key in source) {
        if (Object.prototype.hasOwnProperty.call(source, key)) {
          target[key] = source[key];
        }
      }
    }

    return target;
  };

  return _extends.apply(this, arguments);
}

/**
 * Modal widnow for Bootstrap 4
 * @author Lukáš Černý
 */
var Modal = function (document, window) {

  var EVENTS = {
    HIDE: 'hide',
    HIDE_PRE: 'pre_hide'
  };

  var Modal =
  /*#__PURE__*/
  function () {
    function Modal(id, content, config) {
      this._modal = undefined;
      this._backdrop = undefined;
      config = _extends({
        texts: {},
        config: {},
        elements: {}
      }, config);
      this._config = {
        id: id,
        class: 'modal fade',
        content: content
      };
      this._options = {
        texts: _extends({
          title: 'TITLE',
          closeButton: 'Close'
        }, config.texts),
        config: _extends({
          closeButton: true,
          footerButtons: []
        }, config.config),
        elements: _extends({
          title: 'h4'
        }, config.elements)
      };

      this._generateModal();

      this._generateBackdrop();
    }

    var _proto = Modal.prototype;

    _proto.show = function show() {
      if (typeof this._modal !== 'undefined') {
        document.body.classList.add('open-modal');
        var toShow = this.modal;

        if (!toShow.parentNode || toShow.parentNode.nodeType !== Node.ELEMENT_NODE) {
          document.body.appendChild(toShow);
          document.body.appendChild(this._backdrop);
        }

        toShow.style.display = 'block';
        toShow.classList.add('show');

        this._backdrop.classList.add('show');
      }
    };

    _proto.hide = function hide(event) {
      if (event) {
        event.preventDefault();
      }

      if (typeof this._modal !== 'undefined') {
        var toHide = this.modal;
        toHide.style.display = 'none';
        this._backdrop.style.display = 'none';
        toHide.setAttribute('aria-hidden', true);

        this._backdrop.setAttribute('aria-hidden', true);

        document.body.classList.remove('modal-open');
        var newEvent = document.createEvent('Event');
        newEvent.initEvent(Modal.eventHide, true, true);
        toHide.dispatchEvent(newEvent);
        toHide.remove();

        this._backdrop.remove();
      }
    };

    _proto._generateModal = function _generateModal() {
      var structure = {
        title: document.createElement(this._options.elements.title),
        header: document.createElement('div'),
        body: document.createElement('div'),
        footer: document.createElement('div'),
        content: document.createElement('div'),
        dialog: document.createElement('div')
      };

      for (var key in structure) {
        structure[key].className = 'modal-' + key;
      }

      var that = this;
      structure.title.innerText = this._options.texts.title;
      structure.header.appendChild(structure.title);
      var closeButton = document.createElement('button');
      closeButton.setAttribute('type', 'button');
      closeButton.setAttribute('class', 'close');
      closeButton.dataset.dismiss = 'modal';
      closeButton.innerHTML = '&times;';
      closeButton.addEventListener('click', function () {
        this.hide();
      }.bind(this));
      structure.header.appendChild(closeButton);

      if (typeof this._config.content === 'string') {
        structure.body.innerHTML = this._config.content;
      } else {
        structure.body.appendChild(this._config.content);
      }

      structure.content.appendChild(structure.header);
      structure.content.appendChild(structure.body);
      structure.content.appendChild(structure.footer);
      structure.dialog.appendChild(structure.content);
      structure.modal = document.createElement('div');
      structure.modal.className = this._config.class;
      structure.modal.setAttribute('id', this._config.id);
      structure.modal.appendChild(structure.dialog);

      this._options.config.footerButtons.forEach(function (button) {
        structure.footer.appendChild(button);
      });

      if (this._options.config.closeButton) {
        var closeButtonFooter = document.createElement('button');
        closeButtonFooter.setAttribute('type', 'button');
        closeButtonFooter.className = 'btn btn-default';
        closeButtonFooter.dataset.dismiss = 'modal';
        closeButtonFooter.innerHTML = this._options.texts.closeButton;
        closeButtonFooter.addEventListener('click', function () {
          that.hide();
        });
        structure.footer.appendChild(closeButtonFooter);
      }

      this._modal = structure;
    };

    _proto._generateBackdrop = function _generateBackdrop() {
      this._backdrop = document.createElement('div');
      this._backdrop.className = 'modal-backdrop fade';
    };

    _createClass(Modal, [{
      key: "modal",
      get: function get() {
        return this._modal.modal;
      }
    }], [{
      key: "eventHide",
      get: function get() {
        return EVENTS.HIDE;
      }
    }, {
      key: "eventHidePre",
      get: function get() {
        return EVENTS.HIDE_PRE;
      }
    }]);

    return Modal;
  }();

  if (typeof window.Modal === 'undefined') {
    window.Modal = Modal;
  }

  return Modal;
}(document, window);

export default Modal;
