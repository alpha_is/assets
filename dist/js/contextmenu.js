/*
 * Alpha plugins - ContextMenu
 * Builded: Sat Oct 20 2018 10:43:23 GMT+0000 (UTC)
 */
function _extends() {
  _extends = Object.assign || function (target) {
    for (var i = 1; i < arguments.length; i++) {
      var source = arguments[i];

      for (var key in source) {
        if (Object.prototype.hasOwnProperty.call(source, key)) {
          target[key] = source[key];
        }
      }
    }

    return target;
  };

  return _extends.apply(this, arguments);
}

function _inheritsLoose(subClass, superClass) {
  subClass.prototype = Object.create(superClass.prototype);
  subClass.prototype.constructor = subClass;
  subClass.__proto__ = superClass;
}

/**
 * Base function for support Nette framework
 * @author Lukáš Černý
 */
var Nette = function (document, window) {
  var Nette =
  /*#__PURE__*/
  function () {
    function Nette() {}

    Nette.parseResponse = function parseResponse(response) {
      if (response.snippets) {
        Nette.updateSnippets(response.snippets);
      }

      if (response.redirect) {
        Nette.redirect(response.redirect);
      }
    };

    Nette.redirect = function redirect(location) {
      document.location.replace(location);
    };

    Nette.updateSnippets = function updateSnippets(snippets) {
      for (var snippet in snippets) {
        var $el = document.getElementById(snippet);
        updateSnippet($el, snippets[snippet]);
      }

      function updateSnippet($el, html) {
        if ($el.querySelectorAll('title').length) {
          document.title = html;
        } else {
          if ($el.querySelectorAll('[data-ajax-append]').length) {
            $el.append(html);
          } else if ($el.querySelectorAll('[data-ajax-prepend]').length) {
            $el.prepend(html);
          } else {
            $el.innerHTML = html;
          }
        }
      }
    };

    return Nette;
  }();

  if (typeof window.Nette === 'undefined') {
    window.Nette = Nette;
  }

  return Nette;
}(document, window);

/**
 * Base functions
 * @author Lukáš Černý
 */

var Base = function (document, window) {
  var Base =
  /*#__PURE__*/
  function () {
    function Base() {}

    var _proto = Base.prototype;

    _proto.sendRequest = function sendRequest(config) {
      config = _extends({
        data: {},
        type: 'POST',
        async: true,
        contentType: null,
        success: undefined,
        error: undefined,
        ajax: true
      }, config);
      var xhr;

      if (window.XMLHttpRequest) {
        xhr = new XMLHttpRequest();
      } else {
        xhr = new window.ActiveXObject('Microsoft.XMLHTTP');
      }

      xhr.open(config.type, config.destination, config.async);

      if (config.contentType) {
        xhr.setRequestHeader('Content-Type', config.contentType);
      }

      if (config.ajax) {
        xhr.setRequestHeader('X-Requested-With', 'XMLHttpRequest');
      }

      xhr.onload = function () {
        if (xhr.status === 200) {
          var response = JSON.parse(xhr.responseText);
          Nette.parseResponse(response);

          if (config.success !== undefined) {
            config.success(xhr.responseText);
          }
        } else {
          if (config.error !== undefined) {
            config.error(xhr.responseText);
          }
        }
      };

      if (config.type === 'POST') {
        var data = '';
        if (config.contentType === 'application/json') data = JSON.stringify(config.data);else data = config.data;
        xhr.send(data);
      } else {
        xhr.send();
      }
    };

    _proto.addEventListener = function addEventListener(invokedOn, name, callback, context) {
      if (typeof callback !== 'function') {
        return;
      }

      if (document.addEventListener) {
        invokedOn.addEventListener(name, function (event) {
          callback.apply(context, arguments);
          event.preventDefault();
          return false;
        }, false);
      } else {
        invokedOn.attachEvents('on' + name, function () {
          callback.apply(context, arguments);
          window.event.returnValue = false;
        }, false);
      }
    };

    _proto.removeEventListener = function removeEventListener(invokedOn, name, callback, context) {
      if (typeof callback !== 'function') {
        return;
      }

      if (document.removeEventListener) {
        invokedOn.removeEventListener(name, callback.bind(context), false);
      } else {
        invokedOn.attachEvents('on' + name, function () {
          callback.apply(context, arguments);
          window.event.returnValue = false;
        }, false);
      }
    };

    return Base;
  }();

  return Base;
}(document, window);

var ContextMenu = function (document, window) {
  var ContextMenu =
  /*#__PURE__*/
  function (_Base) {
    _inheritsLoose(ContextMenu, _Base);

    function ContextMenu(invokedOn, config) {
      var _this;

      _this = _Base.call(this) || this;
      _this.position = _extends({
        left: 0,
        top: 0
      }, config.position);
      _this.items = _extends([], config.items);
      _this.invokedOn = invokedOn;
      _this.contextMenu = undefined;
      _this.attached = false;

      _this._init();

      return _this;
    }

    var _proto = ContextMenu.prototype;

    _proto._init = function _init() {
      var ul = this._getTemplate('list');

      this.contextMenu = ul;
      ul.setAttribute('role', 'menu');
      ul.style.display = 'none';
      this.items.forEach(function (action) {
        var item;

        if (action.type === 'divider') {
          item = this._getTemplate('divider');
        } else {
          item = this._getTemplate('item');

          var text = this._getTemplate('innerText'),
              span = this._getTemplate('icon');

          if (action.class) {
            action.class.split(' ').forEach(function (className) {
              item.classList.add(className);
            });
          }

          text.innerText = action.text;

          if (action.icon) {
            action.icon.split(' ').forEach(function (className) {
              span.classList.add(className);
            });
          }

          item.appendChild(span);
          item.appendChild(text);
          this.addEventListener(item, 'click', function (e) {
            if (action.callback) {
              action.callback.apply(this, [this.invokedOn, action.id]);
            }

            this._clickHandler(e);
          }.bind(this), this);
        }

        ul.appendChild(item);
      }.bind(this));
      this.addEventListener(this.contextMenu, 'contextmenu', function () {});
    };

    _proto._getTemplate = function _getTemplate(type) {
      var template = {
        list: {
          elem: 'ul',
          class: 'contxt-menu dropdown-menu'
        },
        item: {
          elem: 'li',
          class: 'dropdown-item'
        },
        divider: {
          elem: 'li',
          class: 'dropdown-divider'
        },
        icon: {
          elem: 'span',
          class: 'icon'
        },
        innerText: {
          elem: 'span',
          class: ''
        }
      };
      var elem = document.createElement(template[type].elem);
      elem.className = template[type].class;
      return elem;
    };

    _proto._clickHandler = function _clickHandler(event) {
      this.destroy();
    };

    _proto.show = function show() {
      if (this.attached) return;
      document.body.appendChild(this.contextMenu);
      this.contextMenu.style.display = 'block';
      this.contextMenu.style.position = 'absolute';
      this.contextMenu.style.left = this.position.left + 'px';
      this.contextMenu.style.top = this.position.top + 'px';
      this.attached = true;
      this.addEventListener(document, 'click', this.destroy, this);
    };

    _proto.hide = function hide() {
      if (!this.attached) return;
      this.removeEventListener(document, 'click', this.destroy, this);
      this.contextMenu.style.display = 'none';
      this.contextMenu.remove();
      this.attached = false;
    };

    _proto.destroy = function destroy() {
      this.hide();
      this.contextMenu = undefined;
    };

    return ContextMenu;
  }(Base);

  if (typeof window.ContextMenu === 'undefined') {
    window.ContextMenu = ContextMenu;
  }

  return ContextMenu;
}(document, window);

export default ContextMenu;
