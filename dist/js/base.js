/*
 * Alpha plugins - Base
 * Builded: Sat Oct 20 2018 10:43:23 GMT+0000 (UTC)
 */
function _extends() {
  _extends = Object.assign || function (target) {
    for (var i = 1; i < arguments.length; i++) {
      var source = arguments[i];

      for (var key in source) {
        if (Object.prototype.hasOwnProperty.call(source, key)) {
          target[key] = source[key];
        }
      }
    }

    return target;
  };

  return _extends.apply(this, arguments);
}

/**
 * Base function for support Nette framework
 * @author Lukáš Černý
 */
var Nette = function (document, window) {
  var Nette =
  /*#__PURE__*/
  function () {
    function Nette() {}

    Nette.parseResponse = function parseResponse(response) {
      if (response.snippets) {
        Nette.updateSnippets(response.snippets);
      }

      if (response.redirect) {
        Nette.redirect(response.redirect);
      }
    };

    Nette.redirect = function redirect(location) {
      document.location.replace(location);
    };

    Nette.updateSnippets = function updateSnippets(snippets) {
      for (var snippet in snippets) {
        var $el = document.getElementById(snippet);
        updateSnippet($el, snippets[snippet]);
      }

      function updateSnippet($el, html) {
        if ($el.querySelectorAll('title').length) {
          document.title = html;
        } else {
          if ($el.querySelectorAll('[data-ajax-append]').length) {
            $el.append(html);
          } else if ($el.querySelectorAll('[data-ajax-prepend]').length) {
            $el.prepend(html);
          } else {
            $el.innerHTML = html;
          }
        }
      }
    };

    return Nette;
  }();

  if (typeof window.Nette === 'undefined') {
    window.Nette = Nette;
  }

  return Nette;
}(document, window);

/**
 * Base functions
 * @author Lukáš Černý
 */

var Base = function (document, window) {
  var Base =
  /*#__PURE__*/
  function () {
    function Base() {}

    var _proto = Base.prototype;

    _proto.sendRequest = function sendRequest(config) {
      config = _extends({
        data: {},
        type: 'POST',
        async: true,
        contentType: null,
        success: undefined,
        error: undefined,
        ajax: true
      }, config);
      var xhr;

      if (window.XMLHttpRequest) {
        xhr = new XMLHttpRequest();
      } else {
        xhr = new window.ActiveXObject('Microsoft.XMLHTTP');
      }

      xhr.open(config.type, config.destination, config.async);

      if (config.contentType) {
        xhr.setRequestHeader('Content-Type', config.contentType);
      }

      if (config.ajax) {
        xhr.setRequestHeader('X-Requested-With', 'XMLHttpRequest');
      }

      xhr.onload = function () {
        if (xhr.status === 200) {
          var response = JSON.parse(xhr.responseText);
          Nette.parseResponse(response);

          if (config.success !== undefined) {
            config.success(xhr.responseText);
          }
        } else {
          if (config.error !== undefined) {
            config.error(xhr.responseText);
          }
        }
      };

      if (config.type === 'POST') {
        var data = '';
        if (config.contentType === 'application/json') data = JSON.stringify(config.data);else data = config.data;
        xhr.send(data);
      } else {
        xhr.send();
      }
    };

    _proto.addEventListener = function addEventListener(invokedOn, name, callback, context) {
      if (typeof callback !== 'function') {
        return;
      }

      if (document.addEventListener) {
        invokedOn.addEventListener(name, function (event) {
          callback.apply(context, arguments);
          event.preventDefault();
          return false;
        }, false);
      } else {
        invokedOn.attachEvents('on' + name, function () {
          callback.apply(context, arguments);
          window.event.returnValue = false;
        }, false);
      }
    };

    _proto.removeEventListener = function removeEventListener(invokedOn, name, callback, context) {
      if (typeof callback !== 'function') {
        return;
      }

      if (document.removeEventListener) {
        invokedOn.removeEventListener(name, callback.bind(context), false);
      } else {
        invokedOn.attachEvents('on' + name, function () {
          callback.apply(context, arguments);
          window.event.returnValue = false;
        }, false);
      }
    };

    return Base;
  }();

  return Base;
}(document, window);

export default Base;
