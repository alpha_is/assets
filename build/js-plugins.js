const path = require('path');
const rollup = require('rollup');
const babel = require('rollup-plugin-babel');
const date = new Date();

const plugins = [
    babel({
        sourceType: 'module',
        presets: [
            [
                '@babel/env',
                {
                    loose: true,
                    modules: false,
                    exclude: ['transform-typeof-symbol']
                }
            ]
        ],
        exclude: 'node_modules/**', // Only this package
        plugins: [
            '@babel/plugin-transform-object-assign',
            '@babel/plugin-proposal-object-rest-spread',
            '@babel/plugin-transform-property-mutators',
            '@babel/plugin-transform-reserved-words',
            'babel-plugin-transform-member-expression-literals',
            'babel-plugin-transform-property-literals'
        ],
    })
];
const source = {
    Base: path.resolve(__dirname, '../src/js/base.js'),
    ContextMenu: path.resolve(__dirname, '../src/js/context_menu.js'),
    Nette: path.resolve(__dirname, '../src/js/nette.js'),
    Modal: path.resolve(__dirname, '../src/js/modal.js')
};

function getBanner(pluginName) {
    return '/*\n'
        + ' * Alpha plugins - ' + pluginName + '\n'
        + ' * Builded: ' + date.toString() + '\n'
        + ' */';
}


function build(plugin) {
    console.log('Building ' + plugin.toString() + ' plugin... ');

    let fileName = plugin.toLowerCase() + '.js';

    rollup.rollup({
        input: source[plugin],
        plugins
    }).then((bundle) => {
        bundle.write({
            banner: getBanner(plugin),
            format: 'esm',
            name: plugin,
            sourcemap: false,
            file: path.resolve(__dirname, '../dist/js/' + fileName)
        });
    }).then(() => {
        console.log('Building ' + plugin.toString() + ' plugin... Done!');
    });
}

Object.keys(source).forEach((plugin) => build(plugin));